import com.datastax.spark.connector.cql.CassandraConnector
import com.datastax.spark.connector._
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession

object Status extends Enumeration {
  val OPEN, CLOSED = Value
}

object Main extends App {


  case class StationData(
                          number: String,
                          name: String,
                          address: String,
                          position: String,
                          banking: Boolean,
                          bonus: Boolean,
                          status: Status.Value,
                          contractName: String,
                          bikeStands: Int,
                          availableBikeStands: Int,
                          availableBikes: Int)

  case class Location(address: String, position: String)

  case class ContractNameLocation(contractName: String, location: Location)

  case class ContractNameCount(contractName: String, count: Int)

  case class ContractName(contractName: String, location: Location, count: Int)

  // CONVERT STATUS
  implicit def convertToStatus(s: String): Status.Value = if (s == "OPEN") Status.OPEN else Status.CLOSED

  //PARSE THE DATA AND CREATE STATIONDATA OBJECT
  def createStationData(line: String): StationData = {
    val data = line.split(";")
    StationData(data(0), data(1), data(2), data(3), data(4).toBoolean, data(5).toBoolean, data(6),
      data(7), data(8).toInt, data(9).toInt, data(10).toInt)
  }

  // CREATE KEYSPACE JCDECAUX AND THE TABLES STATIONSDATA, CONTRACTNAMES
  def init() = {
    cassandraConnector.withSessionDo { session =>
      session.execute("CREATE KEYSPACE IF NOT EXISTS jcdecaux WITH REPLICATION = {'class': 'SimpleStrategy', 'replication_factor': 1};")
      session.execute("CREATE TABLE IF NOT EXISTS jcdecaux.stationsdata (number text, name text, address text, position text, " +
        "banking boolean, bonus boolean, status text, contract_name text, bike_stands int, available_bike_stands int, " +
        "available_bikes int, primary key (contract_name, number));")
      session.execute("CREATE TABLE IF NOT EXISTS jcdecaux.contractname_count (contract_name text, count int, primary key " +
        "(contract_name, count))")
      session.execute("CREATE TYPE IF NOT EXISTS jcdecaux.location (address text, position text)")
      session.execute("CREATE TABLE IF NOT EXISTS jcdecaux.contractname_locations (contract_name text, " +
        "location FROZEN<location>, primary key (contract_name, location))")
      session.execute("CREATE TABLE IF NOT EXISTS jcdecaux.contractnames (contract_name text, " +
        "location FROZEN<location>, count int, primary key ((contract_name, location), count))")
    }
  }

  def createStationsDataRdd(): RDD[StationData] = {
    val lines = sc.textFile("src/main/resources/jcdecaux_bike_data.csv")
    val header = lines.first()
    val data = lines.filter(row => row != header)
    data.map(createStationData)
  }

  def createContractNamesTable(): Unit = {
    val contractNameCountTable = sc.cassandraTable[ContractNameCount]("jcdecaux", "contractname_count")
      .where("count > 99")
      .cache
    val contractNameTable = contractNameCountTable.joinWithCassandraTable("jcdecaux", "contractname_locations")
      .cache
    val countByContractName = contractNameCountTable.keyBy(_.contractName)
    val locationByContractName = contractNameLocations.keyBy(_.contractName)
    val joined = countByContractName.join(locationByContractName).cache
    val contractNameRdd = joined map { data => ContractName(data._1, data._2._2.location, data._2._1.count) }

    contractNameRdd.saveToCassandra("jcdecaux", "contractnames", SomeColumns("contract_name", "location", "count"))
  }

  // Spark Conf with local connection to Cassandra
  val conf: SparkConf = new SparkConf()
    .setMaster("local[*]")
    .setAppName("Spark Cassandra")
    .set("spark.cassandra.connection.host", "127.0.0.1")
    .set("spark.cassandra.auth.username", "cassandra")
    .set("spark.cassandra.auth.password", "cassandra")
  // Spark Context
  val sc: SparkContext = new SparkContext(conf)
  // Spark Connector
  val cassandraConnector = CassandraConnector(conf)

  /*
   * PART 1 (READ WITH SPARK AND SAVE TO CASSANDRA)
  */
  // INIT CASSANDRA
  init()

  // READ FILE AND CREATE STATIONDATARDD
  val stationDataRdd = createStationsDataRdd().cache

  // LENGTH
  println("Dataset Length : " + stationDataRdd.count())

  stationDataRdd.saveToCassandra("jcdecaux", "stationsdata", SomeColumns("number", "name",
  "address", "position", "banking", "bonus", "status", "contract_name", "bike_stands", "available_bike_stands",
  "available_bikes"))

  val contractNameLocations = stationDataRdd map { data => ContractNameLocation(data.contractName, Location(data.address, data.position)) }

  contractNameLocations.saveToCassandra("jcdecaux", "contractname_locations",
    SomeColumns("contract_name" as "contractName", "location"))

  val totalsByContractName = stationDataRdd map { data => (data.contractName, 1) } reduceByKey(_ + _)

  totalsByContractName.saveToCassandra("jcdecaux", "contractname_count")

  /*
   * PART 2 (JOIN TABLES IN CASSANDRA)
   */

  createContractNamesTable()

  /*
   * PART 3 (GET % BIKES AVAILABILITY)
   */

  val sparkSession = SparkSession.builder().config(conf).getOrCreate()
  val df = sparkSession
    .read
    .format("org.apache.spark.sql.cassandra")
    .options(Map("table" -> "stationsdata", "keyspace" -> "jcdecaux"))
    .load()

  import sparkSession.implicits._
  import org.apache.spark.sql.functions._

  def getTotalBikeStands = udf((availableBikeStand: Int, availableBike: Int) => availableBike + availableBikeStand)
  def getAvailibityPercentage = udf((availableBike: Double, totalBike: Double) => availableBike / totalBike * 100d)

  // AVERAGE BIKES IN A STAND
  df.select(round(avg($"available_bike_stands" + $"available_bikes")) as "average number of bikes in a stand")
  df
    .filter($"status" === "OPEN")
    .withColumn("total_bike_stands", getTotalBikeStands(df("available_bike_stands"), df("available_bikes")))
    .withColumn("availability _in_percentage", round(getAvailibityPercentage($"available_bikes", $"total_bike_stands")))
    .show()
}
